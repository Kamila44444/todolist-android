package com.example.todolist

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.add_modify_task_layout.*
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.text.SimpleDateFormat

import java.util.*


class AddTaskActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {//, TimePickerDialog.OnTimeSetListener {


    var name: String = ""
    var description: String = ""
    var deadline: Date? = null
    var type: String = ""
    var priority: Int = 0
    var actionType = "add"
    private val timeMethod = Task()
    var prevcatFile = "lastcategory.txt"


    private var datePickerDialog: DatePickerDialog? = null
    private var timePickerDialog: TimePickerDialog? = null

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

        deadline = Date(year, month, dayOfMonth)
        deadline!!.minutes = 0
        deadline!!.hours = 0

        fillDate()
        datePickerDialog!!.hide()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_modify_task_layout)
        datePickerDialog = DatePickerDialog(this, this@AddTaskActivity, 2019, 1, 1)
        timePickerDialog = TimePickerDialog(this, fun(_: TimePicker?, hourOfDay: Int, minute: Int) {

            if (deadline == null) {
                deadline = Date()
                Log.d("poka", deadline.toString())
                val dateFormat = SimpleDateFormat("yyyy/MM/dd")
                val dateC = Calendar.getInstance()
                val date2 = dateFormat.parse(dateFormat.format(dateC.time).toString())
                deadline!!.year = date2.year
                Log.d("poka", deadline.toString())
                deadline!!.month = date2.month
                Log.d("poka", deadline.toString())
                deadline!!.date = date2.date
                Log.d("poka", deadline.toString())
            }
            Log.d("poka", deadline.toString())
            deadline!!.hours = hourOfDay
            Log.d("poka", deadline.toString())
            deadline!!.minutes = minute

            fillTime()
            fillDate()
            timePickerDialog!!.hide()
        }, 0, 0, true)

        findViewById<SeekBar>(R.id.selectPriority).setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                print("dot")
                //To change body of created functions use File | Settings | File Templates.
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                print("odt")
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val num = selectPriority.progress
                when (num) {
                    3 -> priorityView.setImageResource(R.drawable.r)
                    2 -> priorityView.setImageResource(R.drawable.y)
                    else -> priorityView.setImageResource(R.drawable.g)
                }
            }
        })
        selectDate.setOnClickListener { datePickerDialog!!.show() }
        selectTime.setOnClickListener { timePickerDialog!!.show() }
        timeView.setOnLongClickListener {
            if (this.datePickerDialog != null) {
                this.datePickerDialog!!.show()
                return@setOnLongClickListener true
            }
            return@setOnLongClickListener false
        }
        dateView.setOnLongClickListener {
            if (this.timePickerDialog != null) {
                this.timePickerDialog!!.show();
                return@setOnLongClickListener true;
            }
            return@setOnLongClickListener false;
        }

        putType.text = SpannableStringBuilder(getSelectedCat())

        actionType = intent.getStringExtra("actionType")

        if (actionType == "add") {
            accept.setOnClickListener { accept("add") }
            remove.visibility = View.GONE
        } else {
            accept.setOnClickListener { accept("modify") }
            remove.visibility = View.VISIBLE
            val task = intent.getSerializableExtra("task") as Task
            name = task.name
            description = task.description
            type = task.type
            deadline = task.deadline
            priority = task.priority
            updateValue()
        }

    }

    fun setSelectedCat(cat: String) {
        try {
            val fileOut = openFileOutput(prevcatFile, Context.MODE_PRIVATE)
            val objectOut = ObjectOutputStream(fileOut)
            objectOut.writeObject(cat)
            objectOut.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getSelectedCat(): String {
        var cat = ""
        try {
            val fileIn = openFileInput(prevcatFile)

            val objectIn = ObjectInputStream(fileIn)
            cat = objectIn.readObject() as String
            objectIn.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return cat
    }

    private fun updateValue() {
        putName.text = SpannableStringBuilder(name)
        putDescription.text = SpannableStringBuilder(description)
        putType.text = SpannableStringBuilder(type)
        selectPriority.progress = priority
        if (deadline != null && deadline!!.hours != 0) fillTime()
        if (deadline != null && deadline!!.year != 0) fillDate()


    }

    private fun fillTime() {
        val str = "${timeMethod.to2String(deadline!!.hours)}:${timeMethod.to2String(deadline!!.minutes)}"
        timeView.text = SpannableStringBuilder(str)
    }

    private fun fillDate() {
        if(deadline!!.year < 200){
            deadline!!.year = 2000 + (deadline!!.year % 100)
        }
        val str = deadline!!.year.toString() + "-" + timeMethod.to2String(deadline!!.month + 1) + "-" + timeMethod.to2String(deadline!!.date)
        dateView.text = SpannableStringBuilder(str)
    }

    private fun accept(action: String) {
        name = putName.text.toString()
        description = putDescription.text.toString()
        type = putType.text.toString()
        setSelectedCat(type)
        priority = selectPriority.progress

        val newTask = Task(name, description, deadline, type, priority)

        back(newTask, action)
    }

    fun cancel(view: View) {
        back(null, actionType)
    }

    fun remove(view: View) {
        back(null, "remove")
    }

    private fun back(task: Task?, action: String) {
        val backIntent = Intent(this, MainActivity::class.java)
        backIntent.putExtra("Task", task)
        backIntent.putExtra("action", action)
        startActivityForResult(backIntent, 123)
    }
}
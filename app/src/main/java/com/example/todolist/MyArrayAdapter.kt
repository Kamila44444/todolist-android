package com.example.todolist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import java.io.ObjectOutputStream
import java.util.*


class MyArrayAdapter(context: Context, private var data: ArrayList<Task>) :
    ArrayAdapter<Task>(context, R.layout.single_list_layout_item, data) {

    // ViewHolder!!

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if (view == null) {
            val inflater = LayoutInflater.from(context)
            view = inflater.inflate(R.layout.single_list_layout_item, parent, false)
            view!!.minimumHeight = 200
        }
        view.findViewById<TextView>(R.id.itemName).text = data[position].name
        view.findViewById<TextView>(R.id.description).text = data[position].description
        view.findViewById<TextView>(R.id.typeName).text = data[position].type
        view.findViewById<TextView>(R.id.dateView).text = data[position].toPrintDate()
        view.findViewById<CheckBox>(R.id.checkBox).isChecked = false

        view.findViewById<CheckBox>(R.id.checkBox).setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                data.removeAt(position)
                saveState()
                notifyDataSetChanged()
            }
        }
        when (data[position].priority) {
            3 -> view.findViewById<ImageView>(R.id.showPriority).setImageResource(R.drawable.r)
            2 -> view.findViewById<ImageView>(R.id.showPriority).setImageResource(R.drawable.y)
            else -> view.findViewById<ImageView>(R.id.showPriority).setImageResource(R.drawable.g)
        }

        if (data[position].isAfterDeadline()) {
            view.setBackgroundColor(Color.rgb(240, 93, 93))
        } else {
            view.setBackgroundColor(Color.rgb(250, 250, 250))
        }
        return view
    }
    fun saveState() {
        try {
            val fileOut = context.openFileOutput("state.ser", Context.MODE_PRIVATE)
            val objectOut = ObjectOutputStream(fileOut)
            objectOut.writeObject(data)
            objectOut.close()
            Log.d("myDebug", "The Object  was succesfully written to a file")

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}
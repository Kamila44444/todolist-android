package com.example.todolist

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_modify_task_layout.*
import java.io.*
import java.util.*



class MainActivity : AppCompatActivity() {

    var tasklist = ArrayList<Task>()
    var typelist = ArrayList<String>()
    var typelistFile = "types.ser"
    var stateFile = "state.ser"
    val selectedTaskFile = "selected.ser"
    var myadapter : MyArrayAdapter? = null
    var typeAdapter : ArrayAdapter<String>? = null
    var tmptasklist = tasklist
    var save : Bundle? = null
    var isFiltred : Boolean = false


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        save = savedInstanceState
        getTypeList()
        if(typelist.size == 0){
            typelist.add("")
        }
        getState()
        sortTasklist()
        logList()
        val newTask = intent.getSerializableExtra("Task")
        val actionType = intent.getStringExtra("action")
        var i : Int = -1
        if (newTask != null) {
            if (actionType == "add") {
                tasklist.add(newTask as Task)
                saveState()
                if(!typelist.contains(newTask.type)) {
                    typelist.add(newTask.type)
                }
                saveTypesList()
                val myintent = Intent(this, MainActivity::class.java)
                startActivityForResult(myintent, 123)

            } else if (actionType == "modify") {
                tasklist.removeAt(getSelectedTask())
                tasklist.add(newTask as Task)
                saveState()
                if(!typelist.contains(newTask.type)) {
                    typelist.add(newTask.type)
                }
                saveTypesList()
                val myintent = Intent(this, MainActivity::class.java)
                startActivityForResult(myintent, 123)
            }
        } else if (actionType == "remove") {
            val tmpTask = getSelectedTask()
            tasklist.removeAt(tmpTask)
            logList()
            saveState()
            val myintent = Intent(this, MainActivity::class.java)
            startActivityForResult(myintent, 123)
        }
        else if (actionType == "remove2") {
            i  = intent.getIntExtra("porition", -1)
            if(i != -1) {
                tasklist.removeAt(i)
                logList()
                saveState()
            }
            val myintent = Intent(this, MainActivity::class.java)
            startActivityForResult(myintent, 123)
        }
        tmptasklist = tasklist
        myadapter = MyArrayAdapter(this, tmptasklist)

        listview.adapter = myadapter


        if (tasklist.isEmpty()) {
            addTestTask()
            Log.d("myDebug", "wczytało")
        } else {
            Log.d("myDebug", "nie wczytało")
        }

        listview.setOnItemLongClickListener { _, _, index, id ->
            setSelectedTask(index)
            myadapter!!.notifyDataSetChanged()
            val myintent = Intent(this, AddTaskActivity::class.java)
            myintent.putExtra("actionType", "modify")
            myintent.putExtra("task", tasklist[index])
            startActivityForResult(myintent, 123)
            true
        }
            typeAdapter = ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, typelist )
            //typeAdapter.setDropDownViewResource(selectType)
            selectType.adapter = typeAdapter
            selectType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedFiltr = selectType.selectedItem.toString()
                if(selectedFiltr.compareTo("") == 0 && !isFiltred) return
                removeFormTmpTaskList(selectedFiltr)

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                print("not")
            }
        }

    }

    private fun removeFormTmpTaskList(selectedFiltr: String) {
        if(selectedFiltr.compareTo("") == 0){
            getState()
            tmptasklist = tasklist
            myadapter = MyArrayAdapter(this, tmptasklist)
            listview.adapter = myadapter
            isFiltred = false
        }
        else {
            tmptasklist = tasklist
            val toRemove = ArrayList<Task>()
            for (e: Task in tmptasklist) {
                if (e.type.compareTo(selectedFiltr) != 0) {
                    toRemove.add(e)
                }
            }
            for(i : Task in toRemove){
                tmptasklist.remove(i)
            }
            isFiltred = true
        }

        if(myadapter != null) myadapter!!.notifyDataSetChanged()
    }

    fun addTaskOnClick(view: View) {
        Toast.makeText(this, "Dziala!", Toast.LENGTH_SHORT).show()
        val myintent = Intent(this, AddTaskActivity::class.java)
        myintent.putExtra("actionType", "add")
        //myintent.putExtra("task", null )
        startActivityForResult(myintent, 123)
    }

    fun saveState() {
        try {
            val fileOut = openFileOutput(stateFile, Context.MODE_PRIVATE)
            val objectOut = ObjectOutputStream(fileOut)
            sortTasklist()
            Log.d("stanList", " po sorcie")
            logList()
            objectOut.writeObject(tasklist)
            objectOut.close()
            Log.d("myDebug", "The Object  was succesfully written to a file")

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getState() {
        try {
            val fileIn = openFileInput(stateFile)

            val objectIn = ObjectInputStream(fileIn)
            tasklist = objectIn.readObject() as ArrayList<Task>
            sortTasklist()
            Log.d("stanList", " po sorcie")
            logList()
            objectIn.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        if(myadapter != null) myadapter!!.notifyDataSetChanged()
    }
    fun sortTasklist(){
        if(tasklist.size == 0) return
        for(i : Int in 0..(tasklist.size - 1)){
            var max = tasklist[i].deadline
            var maxTask : Task = tasklist[i]
            var toRemove : Int = i
            Log.d("stanList", max.toString())
            for(j : Int in i..(tasklist.size - 1)){
                if(max == null) break
                if(tasklist[j].deadline == null){
                    max = null
                    maxTask = tasklist[j]
                    toRemove = j
                    Log.d("stanList", max.toString())
                    break
                }
                if(tasklist[j].deadline!!.before(maxTask.deadline)){
                    toRemove = j
                    max = tasklist[j].deadline
                    maxTask = tasklist[j]

                    Log.d("stanList", max.toString())
                }
            }
            tasklist.removeAt(toRemove)
            tasklist.add(i, maxTask)
            logList()
        }
    }

    fun saveTypesList() {
        try {
            val fileOut = openFileOutput(typelistFile, Context.MODE_PRIVATE)
            val objectOut = ObjectOutputStream(fileOut)
            objectOut.writeObject(typelist)
            objectOut.close()
            Log.d("myDebug", "The Object  was succesfully written to a file")

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getTypeList() {
        try {
            val fileIn = openFileInput(typelistFile)

            val objectIn = ObjectInputStream(fileIn)
            typelist = objectIn.readObject() as ArrayList<String>
            objectIn.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        if(typeAdapter != null) typeAdapter!!.notifyDataSetChanged()
    }

    fun logList(){
        var str = ""
        for(e : Task in tasklist){
            str += e.name
        }
        Log.d("stanList", str)
    }

    fun setSelectedTask(index: Int) {
        try {
            val fileOut = openFileOutput(selectedTaskFile, Context.MODE_PRIVATE)
            val objectOut = ObjectOutputStream(fileOut)
            objectOut.writeObject(index)
            objectOut.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun getSelectedTask(): Int {
        var index = -1
        try {
            val fileIn = openFileInput(selectedTaskFile)

            val objectIn = ObjectInputStream(fileIn)
            index = objectIn.readObject() as Int
            objectIn.close()

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return index
    }


    fun addTestTask() {
        val task1 = Task("1", "Bardzo bardzo długi opis ciekawe co się stanie, i przy okacji kup poduszkę", Date(2019, 9, 15), "kot", 1)
        val task2 = Task("2", "...", Date(2019, 7, 15, 15, 40), "kot", 2)
        val task3 = Task("3", "...", Date(2019, 6, 15), "kot", 3)
        val task4 = Task("4", "...", Date(2019, 5, 15), "kot", 1)
        val task7 = Task("7", "...", Date(2019, 4, 15), "pies", 2)
        val task5 = Task("5", "...", Date(2019, 3, 15), "kot", 3)
        val task6 = Task("6", "...", Date(2019, 2, 15), "kot", 1)
        typelist.add("kot")
        typelist.add("pies")
        saveTypesList()
        tasklist.add(task1)
        tasklist.add(task2)
        tasklist.add(task3)
        tasklist.add(task4)
        tasklist.add(task5)
        tasklist.add(task6)
        tasklist.add(task7)
        saveState()
    }
}


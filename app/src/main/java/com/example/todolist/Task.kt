package com.example.todolist

import android.annotation.SuppressLint
import android.util.Log
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class Task(val name: String, var description: String, var deadline: Date?, var type: String, var priority: Int) :
    Serializable {

    constructor() : this("", "", null, "", 0){
        Log.d("const", "robi się")
    }
    @SuppressLint("SimpleDateFormat")
    fun isAfterDeadline(): Boolean {
        if(deadline == null) return false
        val dateFormat = if(deadline!!.hours != 0)
        {
            SimpleDateFormat("yyyy/MM/dd HH:mm")
        }
        else{
            SimpleDateFormat("yyyy/MM/dd")
        }
        val dateS = toPrintDate()
        val date = dateFormat.parse(dateS)
        val dateC = Calendar.getInstance()
        val date2 = dateFormat.parse(dateFormat.format(dateC.time).toString())
        return date2.after(date)
    }

    fun toPrintDate(): String {
        if(deadline == null){
            return ""
        }
        val dateStr = deadline!!.year.toString() + "/" + to2String(deadline!!.month + 1) + "/" + to2String(deadline!!.date)
        return if(deadline!!.hours == 0){
            dateStr
        } else{
            val timeStr = to2String(deadline!!.hours) + ":" + to2String(deadline!!.minutes)
            "$dateStr $timeStr"
        }
    }

    fun to2String(nr: Int): String {
        if(nr < 10) {
            return "0$nr"
        }
        return nr.toString()
    }


}



